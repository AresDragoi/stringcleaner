inputFile = input("Please enter path of file containing the string to clear spaces out of:\n")

try:
    # read all the contents of input file
    fo = open(inputFile, "r")
    inputString = fo.read()
    # take spaces out
    strippedString = inputString.replace("\n", "")

    # write cleaned string to a new file
    strippedFile = open("strippedFile.txt", 'w')
    strippedFile.write(strippedString)
    print("Cleaned string written to", strippedFile.name)

    strippedFile.close()
    fo.close()
except Exception as exc:
    # output exc message for devs followed by a friendly msg for non-devs
    print( str(exc) )
    print("Check to see if file exists at the specified loc.")


# print(f'You entered {value}')